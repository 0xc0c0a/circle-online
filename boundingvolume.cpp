#include "boundingvolume.h"

#include <GL/glew.h>

co::BoundingVolume::~BoundingVolume() {}

void co::BoundingVolume::draw() const {
	glTranslatef(position.x, position.y, position.z);
}

