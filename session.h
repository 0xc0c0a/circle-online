#pragma once

#include "net_recv.h"
#include "net_send.h"

namespace co {

class Session {
public:
	Session();
	Session(const int &socket);

	int get_socket() const {
		return socket;
	}

	bool active() const {
		return socket != -1;
	}

	/* Simple shortcut that should make the whole API a lot nicer. */

	template<typename T>
	void send(const T &data) {
		co::net_send(socket, data);
	}

	template<typename T>
	T recv() {
		return co::net_recv<T>(socket);
	}

private:
	/* I feel like having a "session" as an immutable thing is the best way to
 	 * go, despite the overhead of object creation. */
	int socket;
};

}
