#include "gamestate.h"

#include <SDL.h>
#include <GL/glew.h>
#include <cassert>

co::GameState::GameState() {}
co::GameState::~GameState() {}

void co::GameState::handle_event(const SDL_Event &event) {
	if (event.type == SDL_QUIT) {
		advance_status(co::GameState::Status::Finished);
	}
}

void co::GameState::update(const float &delta_time) {}
void co::GameState::handle_network() {}

void co::GameState::draw() const {
	// XXX: Do I want this?
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

co::GameState::Status co::GameState::get_status() const {
	return status;
}

void co::GameState::advance_status(const co::GameState::Status &status) {
	// nb. > (mt) is fine, it should stop sloppy code. It won't be a problem
	// once finished states are actually removed from the stack (in a bit).

	assert(static_cast<int>(status) > static_cast<int>(this->status));
	this->status = status;
}

