#pragma once

#include "boundingvolume.h"

namespace co {

class AABBBoundingVolume : public BoundingVolume {
public:
	AABBBoundingVolume(const glm::vec3 &min, const glm::vec3 &max);

	virtual bool colliding(const BoundingVolume 		&other) const override;
	virtual bool colliding(const AABBBoundingVolume 	&other) const override;
	virtual bool colliding(const CylinderBoundingVolume &other) const override;
	virtual bool colliding(const SphereBoundingVolume 	&other) const override;

	virtual void draw() const override;

	/* Relative to position, of course. */
	glm::vec3 min;
	glm::vec3 max;
};

}

