#pragma once

union SDL_Event;

namespace co {

class GameState {
public:
	enum class Status {
		New,
		Working,
		Finished
	};

	GameState();
	virtual ~GameState();

	virtual void handle_event(const SDL_Event &event);
	virtual void update(const float &delta_time);
	virtual void handle_network();
	virtual void draw() const;

	Status 	get_status() const;
	void 	advance_status(const Status &status);

private:
	Status status = Status::New;
};

}
