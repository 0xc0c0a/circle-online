#pragma once

#include <vector>
#include <cassert>

/* TODO: Make compatible with move semantics. */

namespace co {

template<typename T>
class StateMachine {
public:
	virtual ~StateMachine() {}

	/* Elements of the StateMachine are manipulated in a stack-like fashion. */

	void push(const T &state) {
		states.push_back(state);
	}

	/* Overloadable for child-defined internal memory management. */
	virtual void pop() {
		assert(!states.empty());
		states.pop_back();
	}

	T top() const {
		return states.back();
	}

	void change(const T &state) {
		if (!empty()) {
			pop(); /* Swap out top state, if there is one. */
		}
		push(state);
	}

	bool empty() const {
		return states.empty();
	}

	/* However, all elements are accessible and mutable through an iterator
 	 * pass-through to the underlying elements. */

	typedef typename std::vector<T>::reverse_iterator 		iterator;
	typedef typename std::vector<T>::iterator 				reverse_iterator;

	typedef typename std::vector<T>::const_reverse_iterator const_iterator;
	typedef typename std::vector<T>::const_iterator 		const_reverse_iterator;

	iterator begin() {
		return states.rbegin();
	}

	iterator end() {
		return states.rend();
	}

	reverse_iterator rbegin() {
		return states.begin();
	}

	reverse_iterator rend() {
		return states.end();
	}

	const_iterator cbegin() const {
		return states.crbegin();
	}

	const_iterator cend() const {
		return states.crend();
	}

	const_reverse_iterator crbegin() const {
		return states.cbegin();
	}

	const_reverse_iterator crend() const {
		return states.cend();
	}

protected:
	std::vector<T> states;
};

}
