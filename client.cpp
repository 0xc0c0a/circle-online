#include "globals.h"
#include "insist.h"
#include "playgamestate.h"

#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glew.h>
#include <SDL_opengl.h>
#include <GL/glu.h>
#include <cassert>

co::Session 			co::g_session;
co::GameStateMachine	co::g_game_states;

/* I'll move this to globals.h if I need it anywhere else. */
static SDL_Window 		*window = nullptr;

static void init();

static void handle_events();
static void update();
static void handle_network();
static void draw();

int main(int argc, char **argv) {
	init();

	while (true) {
		handle_events();

		co::g_game_states.cascade_remove_finished();
		if (co::g_game_states.empty()) {
			break; /* Avoids a frame delay in closing. */
		}
		co::g_game_states.update_new_to_working();

		update();

		if (co::g_session.active()) {
			handle_network(); /* If we're connected to a server, handle messages from them. */
		}

		draw();
	}

	return EXIT_SUCCESS; /* atexit wraps it all up automagically. */
}

static void init() {
	// FIXME: Make error reporting nicer here; use a defined, well-specified
	// format. This will tie in with insist.

	/* Start SDL. */
	co::insist(SDL_Init(SDL_INIT_VIDEO) == 0, "Failed to initialize SDL: %s\n", SDL_GetError());
	atexit(SDL_Quit);

	/* Start SDL TTF. */
	co::insist(TTF_Init() == 0, "Failed to initialise SDL-TTF: %s\n", TTF_GetError());
	atexit(TTF_Quit);

	/* Targetting OpenGL 2.1: Good balance of usability and functionality. */
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	/* Regular OpenGL attributes. */
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	/* Create the window. */
	window = SDL_CreateWindow("(" BUILD_DATE ")", SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL |
		SDL_WINDOW_SHOWN);
	co::insist(window, "Failed to create window: %s\n", SDL_GetError());

	/* Make an OpenGL context, attached to the window. */
	co::insist(SDL_GL_CreateContext(window), "Failed to create context: %s\n", SDL_GetError());

	/* GLEW; OpenGL loading library to load function pointers for OpenG
	 * functions introduced after 1.1. */
	glewExperimental = GL_TRUE;
	const GLenum status = glewInit();
	co::insist(status == GLEW_OK, "Failed to initialize GLEW: %s\n", glewGetErrorString(status));

	/* Synchronize the swap interval with the monitor refresh rate using late
 	 * swap tearing, which works the same as Vsync, unless you've missed the
 	 * vertical retrace for the current frame, where it swaps immediately
 	 * instead. */
	if (SDL_GL_SetSwapInterval(-1) == -1) {
		/* If late swap tearing is unsupported, settle for Vsync. */
		co::insist(SDL_GL_SetSwapInterval(1) == 0, "Failed to SetSwapInterval: %s\n", SDL_GetError());
	}

	/* OpenGL initialization. */
	glClearColor(1.f, 0.f, 1.f, 1.f);

	// TODO: Push first real state.
	co::g_game_states.push(new co::PlayGameState());
}

static void handle_events() {
	for (SDL_Event event; SDL_PollEvent(&event);) {
		for (auto it = co::g_game_states.rbegin(); it != co::g_game_states.rend(); ++it) {
			(*it)->handle_event(event);
		}
	}
}

static void update() {
	const float delta_time = 0.0f; // TODO: Calc.

	for (auto it = co::g_game_states.rbegin(); it != co::g_game_states.rend(); ++it) {
		(*it)->update(delta_time);
	}
}

static void handle_network() {
	// TODO: Receive messages, one at a time (like SDL events).
		// TODO: Pass to all states, bottom-to-top.
}

static void draw() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	co::g_game_states.top()->draw();

	SDL_GL_SwapWindow(window);
}
