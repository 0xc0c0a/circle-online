#pragma once

#ifdef CLIENT

#include "gamestatemachine.h"
#include "session.h"

#elif defined(SERVER)



#endif

namespace co {
#ifdef CLIENT

/* Everything needs to communicate over this. Not the nicest solution, but I
 * can't be bothered with the nicest solution. */
extern Session				g_session;

/* States need to change this. */
extern GameStateMachine		g_game_states;

#elif defined(SERVER)



#endif
}
