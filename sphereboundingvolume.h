#pragma once

#include "boundingvolume.h"

namespace co {

class AABBBoundingVolume;
class CylinderBoundingVolume;

class SphereBoundingVolume : public BoundingVolume {
public:
	SphereBoundingVolume(const double &radius);

	virtual bool colliding(const BoundingVolume 		&other) const override;
	virtual bool colliding(const AABBBoundingVolume 	&other) const override;
	virtual bool colliding(const CylinderBoundingVolume &other) const override;
	virtual bool colliding(const SphereBoundingVolume 	&other) const override;

	virtual void draw() const override;

	double radius;
};

}

