#pragma once

#include "errorgamestate.h"
#include "gamestate.h"
#include "globals.h"

struct addrinfo;
union SDL_Event;

namespace co {

class SessionGameState : public GameState {
public:
	SessionGameState(const char *node, const char *service);
	~SessionGameState();

	virtual void handle_event(const SDL_Event &event) override;
	virtual void handle_network() override;

	template<typename T>
	void abort_unless(const T &expression, const char *format, ...) {
		if (!expression) {
			const std::string message_header("Session error: ");

			// TODO: Use format and varargs above to build a string, passed to
			// the pushed ErrorGameState.

			co::g_game_states.push(new co::ErrorGameState(message_header+"TODO."));
			advance_status(co::GameState::Status::Finished);
		}
	}

private:
	struct addrinfo *getaddrinfo_res = nullptr;
};

}
