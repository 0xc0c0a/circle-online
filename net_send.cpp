#include "net_send.h"

#include "insist.h"

#include <cerrno>
#include <cstring>

template<>
void co::net_send<uint32_t>(const int &sockfd, const uint32_t &data) {
	const uint32_t nl = htonl(data);
	co::insist(send(sockfd, &nl, sizeof(uint32_t), 0) != -1,
		"net_send failed: %s\n", std::strerror(errno));
}

template<>
void co::net_send<int32_t>(const int &sockfd, const int32_t &data) {
	net_send<uint32_t>(sockfd, static_cast<uint32_t>(data));
}

template<>
void co::net_send<string>(const int &sockfd, const string &data) {
	const std::size_t length = data.length();
	co::insist(length <= UINT32_MAX, "Tried to net_send an exceptionally long string.\n");

	co::insist(send(sockfd, data.data(), length, 0) != -1,
		"net_send failed: %s\n", std::strerror(errno));
}

