# Circle Online

_(Now with *absolutely nothing* to do with circles.)_

Bones of a multiplayer game I never finished, written adhering to C++11 practices
(mostly), using raw berkeley sockets and OpenGL 2.1.

**GLEW** for OGL function declarations. **SDL2** for keyboard events and window/GL
context creation. Fonts rendered with **SDL-TTF**.

### Contributors

- Zak Stephens