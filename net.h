#pragma once

#include "net_platform_headers.h"

#include "net_send.h"
#include "net_recv.h"

namespace co {

void net_startup();
void net_cleanup();

/* Does the busywork of filling out a struct addrinfo for hints, etc. for us. */
int net_getaddrinfo(const char *node, const char *service, const int &flags,
	const int &socktype, struct addrinfo **res);

}
