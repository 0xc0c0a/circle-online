#include "session.h"

/* For now, I'm leaving the nitty-gritty connection details up to the
 * individual implementations; we just take a socket, and call it a session. We
 * can hold related data in this object (e.g. username), and use this object to
 * send a receive data (API shortcuts). */

co::Session::Session() : Session(-1) {}
co::Session::Session(const int &socket) : socket(socket) {}
