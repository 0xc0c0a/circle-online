#pragma once

#include <glm/vec3.hpp>

namespace co {

class AABBBoundingVolume;
class CylinderBoundingVolume;
class SphereBoundingVolume;

class BoundingVolume {
public:
	virtual ~BoundingVolume();

	virtual bool colliding(const BoundingVolume 		&other) const = 0;
	virtual bool colliding(const AABBBoundingVolume 	&other) const = 0;
	virtual bool colliding(const CylinderBoundingVolume &other) const = 0;
	virtual bool colliding(const SphereBoundingVolume	&other) const = 0;

	virtual void draw() const;

	glm::vec3 position;
};

}

