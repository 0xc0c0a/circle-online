#pragma once

#include "boundingvolume.h"

union SDL_Event;

namespace co {

class Entity {
public:
	enum class Status {
		Working,
		Finished
	};

	Entity();
	Entity(BoundingVolume *bounding_volume);
	virtual ~Entity();

	virtual void handle_event(const SDL_Event &event);
	virtual void update(const float &delta_time);
	virtual void handle_network();
	virtual void draw() const;

	Status				get_status() const;

	BoundingVolume 		*get_bounding_volume() const;
	glm::vec3 			get_position() const;

	virtual void 		set_position(const glm::vec3 &position);

	void				finish();

private:
	Status				status = Status::Working;

	BoundingVolume 		*bounding_volume = nullptr;
	glm::vec3 			position;

	// TODO: Entry for shader to render with (nullptr for fixed function).
};

}

