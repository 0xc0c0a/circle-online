#pragma once

#include <cstdarg>
#include <cstdio>
#include <cstdlib>

namespace co {

template<typename T>
void insist(const T &expression, const char *format, ...) {
	if (!expression) {
		va_list vargs;
		va_start(vargs, format);

		vfprintf(stderr, format, vargs);

		va_end(vargs);

		exit(EXIT_FAILURE);
	}
}

template<typename T>
void insist(const T &expression) {
	insist(expression, "Insistance failure.");
}

}

