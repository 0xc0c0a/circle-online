#include "net_recv.h"

#include "insist.h"

#include <vector>

#include <cerrno>
#include <cstring>

template<>
uint32_t co::net_recv<uint32_t>(const int &sockfd) {
	uint32_t data;
	co::insist(recv(sockfd, &data, sizeof(uint32_t), 0) != -1,
		"net_recv failed: %s\n", std::strerror(errno));
	return ntohl(data);
}

template<>
int32_t co::net_recv<int32_t>(const int &sockfd) {
	return static_cast<int32_t>(net_recv<uint32_t>(sockfd));
}

template<>
string co::net_recv<string>(const int &sockfd) {
	const uint32_t length = net_recv<uint32_t>(sockfd);
	co::insist(length <= UINT32_MAX, "Tried to net_recv an exceptionally long string.\n");

	std::vector<char> buffer;
	buffer.resize(length);

	co::insist(recv(sockfd, buffer.data(), length, 0) != -1,
		"net_recv failed: %s\n", std::strerror(errno));

	return string(buffer.begin(), buffer.end());
}

