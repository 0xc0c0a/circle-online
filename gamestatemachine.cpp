#include "gamestatemachine.h"

#include <algorithm>

co::GameStateMachine::~GameStateMachine() {
	for (GameState *state : states) {
		delete state;
	}
}

void co::GameStateMachine::pop() {
	delete states.back();
	states.pop_back();
}

void co::GameStateMachine::cascade_remove_finished() {
	/* From first finished state, remove all states that haven't been
 	 * newly-created until the end of the list. */

	const auto end_it = states.end();

	for (auto first_finished_it = states.begin(); first_finished_it != end_it;
		++first_finished_it) {

		if ((*first_finished_it)->get_status() == GameState::Status::Finished) {

			/* Mark all states after (and including) this one that aren't new
 			 * for removal. */
			const auto remove_start_it = std::remove_if(first_finished_it,
				end_it, [](GameState *state)->bool {
					const GameState::Status status = state->get_status();
					return status != GameState::Status::New;
				}
			);

			/* Free all to be removed. */
			for (auto remove_it = remove_start_it; remove_it != end_it;
				++remove_it) {
				delete (*remove_it);
			}

			/* Erase all to be removed from the list. */
			states.erase(remove_start_it, end_it);

			break;
		}
	}
}

void co::GameStateMachine::update_new_to_working() {
	for (GameState *state : states) {
		if (state->get_status() == GameState::Status::New) {
			state->advance_status(GameState::Status::Working);
		}
	}
}

