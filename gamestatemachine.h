#pragma once

#include "gamestate.h"
#include "statemachine.h"

namespace co {

class GameStateMachine : public StateMachine<GameState *> {
public:
	virtual ~GameStateMachine() override;

	virtual void pop() override;

	void cascade_remove_finished();
	void update_new_to_working();
};

}
