#include "net.h"

void co::net_startup() {
#ifdef _WIN32

// TODO: WinSock2 compatibility layer.

#endif
}

void co::net_cleanup() {
#ifdef _WIN32

// TODO: WinSock2 compatibility layer.

#endif
}

int co::net_getaddrinfo(const char *node, const char *service,
	const int &flags, const int &socktype, struct addrinfo **res) {

	struct addrinfo hints = {};
	hints.ai_family = AF_UNSPEC; // IPv4/v6 agnostic.
	hints.ai_protocol = 0; // Deduce protocol from socktype.
	hints.ai_flags = flags;
	hints.ai_socktype = socktype;

	return getaddrinfo(node, service, &hints, res);
}

#include "net_send.cpp"
#include "net_recv.cpp"
