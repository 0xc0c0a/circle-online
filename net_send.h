#pragma once

#include "net_platform_headers.h"

#include <string>
#include <type_traits>

#include <cstdint>

using std::uint32_t;
using std::int32_t;
using std::string;

namespace co {

template<typename T>
void net_send(const int &sockfd, const T &data) {
	static_assert(sizeof(T) == -1, "net_send is undefined for the given type.");
}

template<> void net_send<uint32_t>(const int &sockfd, const uint32_t &data);
template<> void net_send<int32_t>(const int &sockfd, const int32_t &data);

template<> void net_send<string>(const int &sockfd, const string &data);

}
