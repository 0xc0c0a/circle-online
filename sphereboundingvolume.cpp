#include "sphereboundingvolume.h"

#include "aabbboundingvolume.h"
#include "cylinderboundingvolume.h"

#include <glm/glm.hpp>

co::SphereBoundingVolume::SphereBoundingVolume(const double &radius) : radius(radius) {}

bool co::SphereBoundingVolume::colliding(const BoundingVolume &other) const {
	return other.colliding(*this);
}

bool co::SphereBoundingVolume::colliding(const AABBBoundingVolume &other) const {
	return false; // TODO: Sphere-AABB intersection test.
}

bool co::SphereBoundingVolume::colliding(const CylinderBoundingVolume &other) const {
	return false; // TODO: Sphere-cylinder intersection test.
}

bool co::SphereBoundingVolume::colliding(const SphereBoundingVolume &other) const {
	return glm::distance(position, other.position) <= (radius + other.radius);
}

void co::SphereBoundingVolume::draw() const {
	// TODO
}
