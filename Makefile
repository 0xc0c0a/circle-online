CXX := clang++
CXXFLAGS := -std=c++11 -pedantic -Wall -g $(shell sdl2-config --cflags)
CXXFLAGS += -DBUILD_DATE=\"$(shell date | sed "s/\ /\\\ /g")\"
LDLIBS := -lm -lSDL2_ttf $(shell sdl2-config --libs) -lGL -lGLU -lGLEW

.PHONY: clean globals.o

SERVER := server
CLIENT := client

all: $(SERVER)
all: $(CLIENT)

# FIXME: Make target rebuild files if client/server is defined differently.

#
# server
#

$(SERVER): CXXFLAGS += -DSERVER
$(SERVER): server.o boundingvolume.o net.o sphereboundingvolume.o
	$(CXX) $(CXXFLAGS) server.o boundingvolume.o net.o sphereboundingvolume.o -o $(SERVER) $(LDLIBS)

server.o: server.cpp
	$(CXX) $(CXXFLAGS) server.cpp -c

#
# client
#

$(CLIENT): CXXFLAGS += -DCLIENT
$(CLIENT): client.o aabbboundingvolume.o boundingvolume.o entity.o errorgamestate.o gamestate.o gamestatemachine.o globals.o net.o playgamestate.o session.o sessiongamestate.o sphereboundingvolume.o tree.o world.o
	$(CXX) $(CXXFLAGS) client.o aabbboundingvolume.o boundingvolume.o entity.o errorgamestate.o gamestate.o gamestatemachine.o globals.o net.o playgamestate.o session.o sessiongamestate.o sphereboundingvolume.o tree.o world.o -o $(CLIENT) $(LDLIBS)

client.o: client.cpp
	$(CXX) $(CXXFLAGS) client.cpp -c

errorgamestate.o: errorgamestate.cpp
	$(CXX) $(CXXFLAGS) errorgamestate.cpp -c

gamestate.o: gamestate.cpp
	$(CXX) $(CXXFLAGS) gamestate.cpp -c

gamestatemachine.o: gamestatemachine.cpp
	$(CXX) $(CXXFLAGS) gamestatemachine.cpp -c

playgamestate.o: playgamestate.cpp
	$(CXX) $(CXXFLAGS) playgamestate.cpp -c

sessiongamestate.o: sessiongamestate.cpp
	$(CXX) $(CXXFLAGS) sessiongamestate.cpp -c

#
# shared
#

aabbboundingvolume.o: aabbboundingvolume.cpp
	$(CXX) $(CXXFLAGS) aabbboundingvolume.cpp -c

boundingvolume.o: boundingvolume.cpp
	$(CXX) $(CXXFLAGS) boundingvolume.cpp -c

entity.o: entity.cpp
	$(CXX) $(CXXFLAGS) entity.cpp -c

globals.o: globals.cpp
	$(CXX) $(CXXFLAGS) globals.cpp -c

net.o: net.cpp
	$(CXX) $(CXXFLAGS) net.cpp -c

session.o: session.cpp
	$(CXX) $(CXXFLAGS) session.cpp -c

sphereboundingvolume.o: sphereboundingvolume.cpp
	$(CXX) $(CXXFLAGS) sphereboundingvolume.cpp -c

statemachine.o: statemachine.cpp
	$(CXX) $(CXXFLAGS) statemachine.cpp -c

tree.o: tree.cpp
	$(CXX) $(CXXFLAGS) tree.cpp -c

world.o: world.cpp
	$(CXX) $(CXXFLAGS) world.cpp -c

#
# misc. targets
#

clean:
	rm -f $(SERVER) $(CLIENT) *.o
