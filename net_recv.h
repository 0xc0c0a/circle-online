#pragma once

#include "net_platform_headers.h"

#include <string>
#include <type_traits>

#include <cstdint>

using std::uint32_t;
using std::int32_t;
using std::string;

namespace co {

template<typename T>
T net_recv(const int &sockfd) {
	static_assert(sizeof(T) == -1, "net_recv is undefined for the given type.");
	return -1;
}

template<> uint32_t net_recv<uint32_t>(const int &sockfd);
template<> int32_t net_recv<int32_t>(const int &sockfd);

template<> string net_recv<string>(const int &sockfd);

}
