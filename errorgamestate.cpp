#include "errorgamestate.h"

co::ErrorGameState::ErrorGameState(const std::string &message) : message(message) {}
co::ErrorGameState::~ErrorGameState() {}

void co::ErrorGameState::handle_event(const SDL_Event &event) {
	GameState::handle_event(event);

	
}

void co::ErrorGameState::draw() const {}

