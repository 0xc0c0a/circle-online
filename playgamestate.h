#pragma once

#include "gamestate.h"
#include "world.h"

namespace co {

class PlayGameState : public GameState {
public:
	PlayGameState();

	virtual void handle_event(const SDL_Event &event) override;
	virtual void update(const float &delta_time) override;
	virtual void handle_network() override;
	virtual void draw() const override;

private:
	World world;
};

}
