#include "entity.h"

co::Entity::Entity() {}
co::Entity::Entity(BoundingVolume *bounding_volume) : bounding_volume(bounding_volume) {}

co::Entity::~Entity() {
	delete bounding_volume;
}

void co::Entity::handle_event(const SDL_Event &event) {}
void co::Entity::update(const float &delta_time) {}
void co::Entity::handle_network() {}

void co::Entity::draw() const {}

co::Entity::Status co::Entity::get_status() const {
	return status;
}

co::BoundingVolume *co::Entity::get_bounding_volume() const {
	return bounding_volume;
}

glm::vec3 co::Entity::get_position() const {
	return position;
}

void co::Entity::set_position(const glm::vec3 &position) {
	this->position = position;

	if (bounding_volume) {
		bounding_volume->position = position;
	}
}

void co::Entity::finish() {
	status = Status::Finished;
}
