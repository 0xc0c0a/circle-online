#pragma once

#include "boundingvolume.h"

namespace co {

class AABBBoundingVolume;
class SphereBoundingVolume;

class CylinderBoundingVolume : public BoundingVolume {
public:
	CylinderBoundingVolume(const double &radius, const double &height);

	virtual bool colliding(const BoundingVolume 		&other) const override;
	virtual bool colliding(const AABBBoundingVolume 	&other) const override;
	virtual bool colliding(const CylinderBoundingVolume &other) const override;
	virtual bool colliding(const SphereBoundingVolume	&other) const override;

	virtual void draw() const override;

	double radius;
	double height;
};

}

