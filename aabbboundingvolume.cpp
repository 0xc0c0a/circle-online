#include "aabbboundingvolume.h"

#include <GL/glew.h>

co::AABBBoundingVolume::AABBBoundingVolume(const glm::vec3 &min, const glm::vec3 &max) : min(min), max(max) {}

bool co::AABBBoundingVolume::colliding(const BoundingVolume &other) const {
	return other.colliding(*this);
}

bool co::AABBBoundingVolume::colliding(const AABBBoundingVolume &other) const {
	return (position.x + min.x < other.position.x + other.max.x && 
			position.y + min.y < other.position.y + other.max.y &&
			position.z + min.z < other.position.z + other.max.z &&

			position.x + max.x > other.position.x + other.min.x &&
			position.y + max.y > other.position.y + other.min.y &&
			position.z + max.z > other.position.z + other.min.z);
}

bool co::AABBBoundingVolume::colliding(const CylinderBoundingVolume &other) const {
	return false; // TODO: Cylinder intersection test.
}

bool co::AABBBoundingVolume::colliding(const SphereBoundingVolume &other) const {
	return false; // TODO: Sphere intersection test.
}

void co::AABBBoundingVolume::draw() const {
	BoundingVolume::draw();

	// TODO: Create utility function for drawing geometric shapes (quickly).

	glBegin(GL_LINES);

	/* Bottom */
	glVertex3d(min.x, min.y, max.z);
	glVertex3d(max.x, min.y, max.z);

	glVertex3d(max.x, min.y, max.z);
	glVertex3d(max.x, min.y, min.z);

	glVertex3d(max.x, min.y, min.z);
	glVertex3d(min.x, min.y, min.z);

	glVertex3d(min.x, min.y, min.z);
	glVertex3d(min.x, min.y, max.z);

	/* Top */
	glVertex3d(min.x, max.y, max.z);
	glVertex3d(max.x, max.y, max.z);

	glVertex3d(max.x, max.y, max.z);
	glVertex3d(max.x, max.y, min.z);

	glVertex3d(max.x, max.y, min.z);
	glVertex3d(min.x, max.y, min.z);

	glVertex3d(min.x, max.y, min.z);
	glVertex3d(min.x, max.y, max.z);

	/* Sides */
	glVertex3f(min.x, min.y, max.z);
	glVertex3f(min.x, max.y, max.z);

	glVertex3f(max.x, min.y, max.z);
	glVertex3f(max.x, max.y, max.z);

	glVertex3f(max.x, min.y, min.z);
	glVertex3f(max.x, max.y, min.z);

	glVertex3f(min.x, min.y, min.z);
	glVertex3f(min.x, max.y, min.z);

	glEnd();
}

