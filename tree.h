#pragma once

#include "entity.h"

namespace co {

class Tree : public Entity {
public:
	Tree(const double &height);

	virtual void draw() const override;

};

}
