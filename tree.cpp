#include "tree.h"

#include "aabbboundingvolume.h"
#include <GL/glew.h>
#include <cmath>

co::Tree::Tree(const double &height) : Entity(new AABBBoundingVolume(glm::vec3(-0.25, 0, -0.25), glm::vec3(0.25, height, 0.25))) {}

void co::Tree::draw() const {
	glTranslatef(get_position().x, get_position().y, get_position().z);

	const std::size_t 	segments = 12;
	const double 		radius = 0.25;

	glBegin(GL_LINE_LOOP);

	glColor3f(0, 1, 0);

	for (std::size_t i = 0; i < segments; ++i) {
		const double theta = (static_cast<double>(i) / static_cast<double>(segments)) * 2.0 * M_PI;

		const double x = radius * std::cos(theta);
		const double y = radius * std::sin(theta);

		glVertex3f(x, y, 0.0);
	}

	glEnd();

	get_bounding_volume()->draw();
}

