#include "cylinderboundingvolume.h"

#include "aabbboundingvolume.h"
#include "sphereboundingvolume.h"

co::CylinderBoundingVolume::CylinderBoundingVolume(const double &radius, const double &height) : radius(radius), height(height) {}

bool co::CylinderBoundingVolume::colliding(const BoundingVolume &other) const {
	return other.colliding(*this);
}

bool co::CylinderBoundingVolume::colliding(const AABBBoundingVolume &other) const {
	return false; // TODO: AABB intersection test.
}

bool co::CylinderBoundingVolume::colliding(const CylinderBoundingVolume &other) const {
	return false; // TODO: Cylinder intersection test.
}

bool co::CylinderBoundingVolume::colliding(const SphereBoundingVolume &other) const {
	return false; // TODO: Sphere intersection test.
}

void co::CylinderBoundingVolume::draw() const {
	// TODO
}

