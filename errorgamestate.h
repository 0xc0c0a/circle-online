#pragma once

#include "gamestate.h"

#include <string>

namespace co {

class ErrorGameState : public GameState {
public:
	ErrorGameState(const std::string &message);
	~ErrorGameState();

	virtual void handle_event(const SDL_Event &event) override;
	virtual void draw() const override;

private:
	std::string message;
};

}
