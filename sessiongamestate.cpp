#include "sessiongamestate.h"

#include "insist.h"
#include "net.h"

#include <SDL.h>
#include <cstring>

co::SessionGameState::SessionGameState(const char *node, const char *service) {
	co::insist(!co::g_session.active(), "Tried to connect to a server twice!");

	co::net_startup();

	const int gai_status = co::net_getaddrinfo(node, service, 0, SOCK_STREAM, &getaddrinfo_res);
	abort_unless(
		gai_status == 0, "Network address/service translation failed: %s\n",
		gai_strerror(gai_status)
	);

	co::g_session = Session(socket(getaddrinfo_res->ai_family,
		getaddrinfo_res->ai_socktype, getaddrinfo_res->ai_protocol));
	abort_unless(
		co::g_session.active(), "Failed to create socket: %s\n",
		std::strerror(errno)
	);

	abort_unless(
		connect(co::g_session.get_socket(), getaddrinfo_res->ai_addr,
			getaddrinfo_res->ai_addrlen) == 0, "Failed to connect: %s\n",
			std::strerror(errno)
	);
}

co::SessionGameState::~SessionGameState() {
	co::g_session = co::Session();

	freeaddrinfo(getaddrinfo_res);
	co::net_cleanup();
}

void co::SessionGameState::handle_event(const SDL_Event &event) {
	GameState::handle_event(event);

	/* We want to quit, but we're connected to a server. */
	if (event.type == SDL_QUIT && co::g_session.active()) {
		// TODO: Send shutdown message to server.
	}
}

void co::SessionGameState::handle_network() {
	GameState::handle_network();

	// TODO: If the server sent us a "shutdown" message...
	if (!true && false) {
		co::g_game_states.push(new co::ErrorGameState("Server shutting down."));
		advance_status(co::GameState::Status::Finished);
	}
}
