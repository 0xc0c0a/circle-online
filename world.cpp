#include "world.h"

#include "entity.h"
#include <algorithm>
#include <cassert>

co::World::~World() {}

void co::World::update(const float &delta_time) {
	for (Entity *entity : entities) {
		entity->update(delta_time);
	}
}

void co::World::draw() const {
	for (Entity *entity : entities) {
		entity->draw();
	}
}

void co::World::add_entity(Entity *entity) {
	assert(entity != nullptr);
	entities.push_back(entity);
}

void co::World::remove_finished_entities() {
	entities.erase(std::remove_if(entities.begin(), entities.end(),
		[](Entity *entity)->bool {
			return entity->get_status() == Entity::Status::Finished;
		}), entities.end()
	);
}

