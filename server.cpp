#include "sphereboundingvolume.h"

#include <iostream>

int main(int argc, char **argv) {
#if 0
	const char *service = "1337";
	const int backlog = 15;

	co::net_startup();
	std::atexit(co::net_cleanup);

	/* MAKE FUNCTION */

	struct addrinfo *gai_res;
	const int gai_status = co::net_getaddrinfo(nullptr, service, AI_PASSIVE,
		SOCK_STREAM, &gai_res);
	co::insistf(gai_status == 0,
		"Network address/service translation failed: %s\n",
		gai_strerror(gai_status));

	const int server_socket = socket(gai_res->ai_family, gai_res->ai_socktype,
		gai_res->ai_protocol);
	co::insistf(server_socket != -1, "Failed to create socket: %s\n",
		std::strerror(errno));

	co::insistf(bind(server_socket, gai_res->ai_addr, gai_res->ai_addrlen) == 0,
		"Failed to bind socket: %s\n", std::strerror(errno));

	freeaddrinfo(gai_res); // TODO: Error check this.

	/* /MAKE FUNCTION */

	while (true) {

		co::insistf(listen(server_socket, backlog) == 0,
			"Failed to listen: %s\n",
			std::strerror(errno));

		const int client_socket = accept(server_socket, NULL, NULL);
		co::insistf(client_socket != -1, "Failed to accept client: %s\n",
			std::strerror(errno));

		/* *** */

		

		/* *** */

	}
#endif

	co::SphereBoundingVolume sbv1(1.0);
	co::SphereBoundingVolume sbv2(1.0);

	sbv2.position.x += 2.0001;

	std::cout << "Colliding: " << (sbv1.colliding(sbv2) ? "true" : "false") << std::endl;

	return EXIT_SUCCESS;
}
