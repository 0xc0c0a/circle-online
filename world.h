#pragma once

#include <vector>

namespace co {

class Entity;

/* By separating "the world" into a separate class, we can potentially easily
 * keep multiple instances of "the world" in memory. This might be useful
 * later. */

class World {
public:
	~World();

	void update(const float &delta_time);
	void draw() const;

	void add_entity(Entity *entity);
	void remove_finished_entities();

private:
	std::vector<Entity *> entities;
};

}

