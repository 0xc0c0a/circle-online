#include "playgamestate.h"

#include "aabbboundingvolume.h"
#include "tree.h"

co::PlayGameState::PlayGameState() {
	world.add_entity(new Tree(0.5));
}

// TODO: Remove unneeded calls to parent.

void co::PlayGameState::handle_event(const SDL_Event &event) {
	GameState::handle_event(event);


}

void co::PlayGameState::update(const float &delta_time) {
	GameState::update(delta_time);

	world.update(delta_time);

	world.remove_finished_entities(); /* Housekeeping. */
}

void co::PlayGameState::handle_network() {
	GameState::handle_network();
}

void co::PlayGameState::draw() const {
	GameState::draw();

	world.draw();

	// TODO: (Depending on state) draw camera view.
}

// TODO: Things to think about:
	// Camera (and ownership).
	// Player ownership, relation to camera, relation to incoming net-events.

